# Pretest
Solusi bisa dikerjakan dengan bahasa `Java` atau `Kotlin`. Bilamana kurang 
familiar dengan kedua bahasa tersebut, kandidat boleh menuliskan solusinya
dengan pilihan bahasa sebagai berikut:
- C#
- C++
- Go
- Basic
- Javascript
- PHP
- Python
- Swift

## Submission
Solusi dikirimkan melalui email ke 
[erick.pranata@yapos.id](mailto:erick.pranata@yapos.id)
dalam sebuah folder yang sudah di-_compress_.

Usahakan agar file yang dikirimkan hanya berisi source code solusi Anda,
agar ukuran file tidak terlalu besar.

## Pertanyaan? 
Hubungi Erick Pranata di [erick.pranata@yapos.id](mailto:erick.pranata@yapos.id)
bilamana ada yang perlu ditanyakan terkait test ini.
