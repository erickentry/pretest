Perhatikan persamaan berikut ini:

```
5x+2y = 7
```

Jika `-5 <= x <= 5` dan `-5 <= y <= 5`, lengkapi fungsi di bawah ini
sehingga fungsi tersebut bisa mencari 1 pasang nilai `x` dan `y` yang sesuai 
agar persamaan di atas menjadi benar:

```
class Param {
  public int x;
  public int y;
}

Param getParamFor5x2y7() {
  int minX = -5;
  int maxX = 5;
  int minY = -5;
  int maxY = 5;
  ...
}
```
