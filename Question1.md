Suatu sekolah gemar mendata **nama** dan **olahraga favorit** setiap siswa yang baru masuk ke sekolah tersebut agar sekolah tersebut dapat memperoleh informasi mengenai tingkat minat siswa akan tren olahraga tertentu. Sejauh ini, hipotesis yang dikemukakan oleh Kesiswaan mengatakan bahwa Basket, Sepak Bola, dan Renang adalah olahraga yang sedang _nge-trend_, sedangkan olah raga lain tidak termasuk hitungan, sehingga sekolah hanya mencatat minat siswa untuk ketiga olahraga itu saja.

Jika data di atas disimpan dengan format sebagai berikut:
```
val basketballLovers = listOf("ARDI", "RESKA", "SISKA");
val soccerLovers = listOf("SISKA", "BUDI", "DAMIAN");
val swimmingLovers = listOf("DAMIAN", "RESKA", "ANITA");
```

Lengkapilah fungsi-fungsi berikut ini agar outputnya sesuai dengan yang diharapkan (urutan boleh tidak sama):
```
// Output yang diharapkan: ["SISKA"]
List<String> getStudentsWhoLoveBasketballAndSoccer(List<String> basketballLovers, List<String> soccerLovers) {
  ...
}

// Output yang diharapkan: ["ARDI", "RESKA", "SISKA", "DAMIAN", "ANITA"]
List<String> getStudentsWhoLoveBasketballOrSwimming(List<String> basketballLovers, List<String> swimmingLovers) {
  ...
}

// Output yang diharapkan: ["BUDI"]
List<String> getStudentsWhoOnlyLoveSoccer(List<String> basketballLovers, List<String> soccerLovers, List<String> swimmingLovers) {
  ...
}
```
