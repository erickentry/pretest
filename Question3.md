Diberikan class berikut ini:

```
class Node
{
  public int value;
  public Node next;
  public Node(int value)
  {
    this.value = value;
    next = null;
  }
}
```

Lengkapi method bernama `rotateRight` yang didaftarkan pada class `OurList` di bawah ini sehingga sebuah Single Linked List dapat digeser ke kanan 1 kali.

```
class OurList
{
  Node head;
  int length;
  public OurList()
  {
    head = null;
    length = 0;
  }

  public void rotateRight() {
    ...
  }
}
```

_Ilustrasi_

![Linked List Transformation](q3_img.png "Linked List Transformation")